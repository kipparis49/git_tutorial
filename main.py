#!python3

import json
import re

def process_name(name):
    """
    return first-letter-capitalized name. replace "_" and "-" with " "
    """
    replace_dash_re = re.compile(r"[_-]")
    return replace_dash_re.sub(" ", name).title()

def get_name():
    with open("info.json") as fl: # read info from json
        data = json.load(fl)
    name = data.get('name', '') # get name field
    return name

def write_name(name):
    try:
        with open("info.json") as fl: # read info from json
            data = json.load(fl)
        data['name'] = name

        with open("info.json", "w") as fl: # write json back to file
            json.dump(data, fl)
        return True
    except FileNotFoundError:
        return False



if __name__ == "__main__":
    name = get_name()
    print(f"get name:\t{name}")
    processed_name = process_name(name)   # process value
    print(f"processed name:\t{processed_name}")
    # don't write values if they are same
    writed = False if processed_name == name else write_name(processed_name)
    if writed:
        print(f"write name:\t{name}")
    else:
        print(f"skip:\t{name}")

